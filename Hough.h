
#include "TH2F.h"

#include <vector>
#include <array>
#include <map>
#include <utility>

#ifndef HOUGH_H
#define HOUGH_H

#define DBGLV 0

/* ACTAR - TPC parameters: 1, 2 and 0.08 mm */

#define shift 1   
#define pad_size  2
#define pad_pitch 0.08 

class Point;

class Hough : public TObject
{
 private:
    // data
    std::vector<Point> points;  
    
    // found inliers 
    std::vector<Point> inliers; 
    
    // initial number of points of the cloud
    unsigned int _initial_number_points; 
    
    // maximum eucludean distance from the origin
    double _max_distance; 

    // initial energy (charge) associated to an event
    double _initial_energy; 
    
    // maximum distance for a point to be considered an inlier of the detected line
    double _inliers_distance;
    
    // line parameters: y = mx + q
    double m, q;
    
    /* 
        Vector containing the parameters of all the detected lines
        
        quality factor (the lower, the better) 
        slope, 
        intercept, 
        number of fitted points,
        total energy
     */
    std::vector<std::array<double, 5> > lines; 
    
    // Hough-transformed space
    TH2F *hough_space; 
    
    // maximum of hough_space
    std::pair<double, double> hough_maximum;  
            
 public:
    
    // constructors
    Hough();   
    Hough(TH2F *data); 
    Hough(TH2F *data, double inliers_distance);
    
    // destructor
    ~Hough();
    
    /* setters */
    
    void set_data(TH2F *data);
    
    void set_inliers_distance(double inliers_distance){_inliers_distance = inliers_distance;}  
        
    /* getters */
        
    double get_energy(std::vector<Point> point_cloud);
       
    double get_inliers_distance(){return _inliers_distance;}
    
    TH2F *get_hough_space() {return hough_space;}    
    
    std::pair<double, double> get_maximum();
    
    /* 
        Vector containing the parameters of all the detected lines
        
        quality factor (the lower, the better) 
        slope, 
        intercept, 
        number points,
        energy
    */
    std::vector<std::array<double, 5> > get_lines() {return lines;} 
    
    // line detection methods
    
    /*
      create hough_space
      
      arguments:
      
      res_ -> resolution
          dist ---> on distance (input in mm)
          ang  ---> on angle    (input in degrees)
      
      number of bins
                on distance axis = 2*_max_distance/res_dist
                on angle (<-> slope) axis = PI/res_ang (rad)
    */
    void transform(double res_dist, double res_ang);
    
    /* 
      find the bin with maximum value in the Hough space
      and put the related points in the 'inliers' vector
    */
    void find_maximum();  
    
    // find the inliers associated to the detected line
    void find_inliers();
    
    // remove the found inliers 
    void remove_inliers();
    
    // process the data to find all the tracks, i.e. fill the 'lines' vector
    void process(double res_dist, double res_ang, double energy_fraction); 
    
    // show the results
    void print_lines();

};

#endif
