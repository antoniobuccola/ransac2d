// Hough class implementation

#include "Point.h"
#include "Hough.h"

#include "TMath.h"

using namespace std;
using namespace TMath;

Hough::Hough()
{
 _max_distance = 0;
 
 _initial_number_points = 0;
 _initial_energy = 0;
 
 _inliers_distance = 0;
 
 inliers = {};
 lines = {};
 
 hough_space  = NULL;
 
 hough_maximum = make_pair(-1,-1);
 
 _inliers_distance = 0;
}

/***************************************************************************/
/***************************************************************************/

Hough::Hough(TH2F *data)
{
 set_data(data);
 
 _initial_number_points = points.size();
 
 inliers = {};
 lines = {};
 
 hough_space  = NULL;
 
 hough_maximum = make_pair(-1,-1);

 _inliers_distance = 1.5*(pad_size + pad_pitch); 
}

/***************************************************************************/
/***************************************************************************/

Hough::Hough(TH2F *data, double inliers_distance)
{
 set_data(data);
 
 _initial_number_points = points.size();
 
 inliers = {};
 lines = {};
 
 hough_space = NULL;
 
 hough_maximum = make_pair(-1,-1);
 
 _inliers_distance = inliers_distance; 
}

/***************************************************************************/
/***************************************************************************/

void Hough::set_data(TH2F *data)
{
 int nbinsx = data->GetXaxis()->GetNbins();
 int nbinsy = data->GetYaxis()->GetNbins();
 
 for(int xx = 1; xx <= nbinsx; xx++)
 {
  for(int yy = 1; yy <= nbinsy; yy++)
  {
   int cbin = data->GetBin(xx, yy);
   
   double c = data->GetBinContent(cbin);
   if(c>0)
   {      
    double x = shift + (xx-1)*(pad_size+pad_pitch);
    double y = shift + (yy-1)*(pad_size+pad_pitch);
    double e = c;
    
    Point p(x,y,e);
    
    #if DBGLV > 1
        cout<<"set_data "<<p<<endl;
    #endif
    
    points.push_back(p);
   }
  }
 }
 
 _initial_energy = get_energy(points);
 
 double max_x = (nbinsx-1)*(pad_size+pad_pitch);
 double max_y = (nbinsy-1)*(pad_size+pad_pitch);
 
 _max_distance = Sqrt(max_x*max_x + max_y*max_y);
 
 hough_maximum = make_pair(-1,-1);
}

/***************************************************************************/
/***************************************************************************/

Hough::~Hough()
{
 if(hough_space != NULL)
 {
  delete hough_space;
  hough_space = NULL;
 }
 cout<<"here destructor";
}

/***************************************************************************/
/***************************************************************************/

double Hough::get_energy(vector<Point> point_cloud)
{
 double E = 0;
 for(Point p : point_cloud)
     E += p.get_e();
     
 return E;
}

/***************************************************************************/
/***************************************************************************/

void Hough::transform(double res_dist, double res_ang)
{
 res_ang *= DegToRad(); 

 if(hough_space == NULL)
 { 
  int nbins_dist = (int)_max_distance/res_dist;
  int nbins_ang  = (int)Pi()/res_ang; // Pi() returns 3.1415...

  hough_space = new TH2F("hough_space",
                         "Hough-trasformed Space",
                          nbins_ang, 0, Pi(), nbins_dist, -_max_distance, _max_distance);
 }
 else
     hough_space->Reset();

 for(Point p : points) // loop on physical-space points
 {
  #if DBGLV > 1
      cout<<"transform - PH point: "<<p<<endl;
  #endif
  array<double, 3> v = p.get_xye();
  
  double x = v[0];
  double y = v[1];
  double e = v[2];
    
  for(double alpha=0; alpha <= Pi(); alpha += res_ang)
  {
   double d = -x*Sin(alpha) + y*Cos(alpha); // Hough transformation 
   hough_space->Fill(alpha, d, e); 
  }
 }
 // end loop on physical points
 
}

/***************************************************************************/
/***************************************************************************/

void Hough::find_maximum()
{
 if(hough_space == NULL)
 {
  cout<<"Error in Hough::find_maximum - Hough-transformed space not found\n";
  return;
 }
 
 double max = 1; 
 
 // angles in radians, restricted to less than Pi to avoid double counting same lines with slope  = 0
 for(int xx = 1; hough_space->GetXaxis()->GetBinCenter(xx) <= 3.1; xx++)
 {
  for(int yy = 1; yy < hough_space->GetYaxis()->GetNbins(); yy++)
  {
   int idbin = hough_space->GetBin(xx, yy);
   
   double cbin = hough_space->GetBinContent(idbin);
   if(cbin>max)
   {
    #if DBGLV > 1
        cout<<"find_maximum - Bin content: "<<cbin<<endl;
    #endif
    
    hough_maximum.first  = hough_space->GetXaxis()->GetBinCenter(xx); // alpha
    hough_maximum.second = hough_space->GetYaxis()->GetBinCenter(yy); // distance
    
    max = cbin;
   }
  }
 }
 
}

/***************************************************************************/
/***************************************************************************/

pair<double, double> Hough::get_maximum()
{
 if(hough_maximum.first == 0 and hough_maximum.second == 0)
     find_maximum();
 return hough_maximum;
}

/***************************************************************************/
/***************************************************************************/

void Hough::find_inliers()
{ 
 double alpha = hough_maximum.first;
     
 m = Tan(alpha);
 q = hough_maximum.second/Cos(alpha);
 
 for(Point P : points)
 {
  double d = Point::distance(P, m, q);
  if(d <= _inliers_distance)
      inliers.push_back(P);
 }
}

/***************************************************************************/
/***************************************************************************/

void Hough::remove_inliers()	
{
 // this function is called if inliers were found 
 #if DBGLV > 1
     cout<<"Removing inliers\n";
 #endif
 for(Point P : inliers) 
 {
  for(Point Q : points)
  {
   if(P == Q)
   {
    // an inlier has been found and it will be removed from the actual points
    points.erase(remove(points.begin(), points.end(), Q), points.end());
    break;
   }
  }
  //loop on points end
 }
 // loop on inliers end
}

/***************************************************************************/
/***************************************************************************/

void Hough::process(double res_dist, double res_ang, double energy_fraction)
{
 lines = {};
 
 while(true)
 {
  double current_energy = get_energy(points); 
  
  #if DBGLV > 0
      cout<<"process "<<current_energy<<"  "<<current_point_number<<endl;
      cout<<(current_energy >= _initial_energy*energy_fraction)<<"  "<<(current_point_number > 1)<<endl;
  #endif
  
  if(!(current_energy > _initial_energy*energy_fraction))
      break;

  transform(res_dist, res_ang); // transform into the Hough space

  find_maximum(); // assign the Hough maximum of the current Hough space
  find_inliers(); // assign the slope and the intercept of the detected line

  double quality = Point::get_quality(inliers, m, q);
  double line_points = inliers.size();   // needed 'double' declaration for compatibility
  double line_energy = get_energy(inliers);

  lines.push_back({quality, m, q, line_points, line_energy}); 

  remove_inliers();

  inliers = {};
 } 
}

/***************************************************************************/
/***************************************************************************/

void Hough::print_lines()
{
 cout<<"*******************************\n";
 cout<<"        Found "<<lines.size()<<" line(s) "<<endl;
 cout<<"*******************************\n";

 if(lines.size()!=0)
 {
  for(auto l : lines)
  {
   cout<<"\n----------------\n";
   cout<<"Quality: "<<l[0]<<endl;
   cout<<"Slope: "<<l[1]<<endl;
   cout<<"Intercept: "<<l[2]<<endl;
   cout<<"Number of Points: "<<(int)l[3]<<endl;
   cout<<"Charge: "<<l[4]<<endl;
  } 
 }
}




