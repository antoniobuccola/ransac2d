/*
  Class for 2D implementation of RANSAC (RANdom SAmple Consensus)
  
  Some info on the algorithm:
  https://en.wikipedia.org/wiki/Random_sample_consensus
  
  Works using ROOT libraries:
  https://root.cern.ch/
*/

#include "TObject.h"
#include "TH2F.h"

#include <array>
#include <vector>
#include <iostream>

#ifndef Ransac_H
#define Ransac_H

#define DBGLV 0

/* ACTAR - TPC parameters: 1, 2 and 0.08 mm */

#define shift 0   
#define pad_size  1
#define pad_pitch 0

class Point;

class Ransac: public TObject 
{
 private:
     double _d_max;  // maximum euclidean distance to give consensus
     
     int _n_tries;          // maximum number of times the seach of inliers begins     
     int _max_iterations;   // maximum number of times the algorithm "sees" the cloud of points 

     unsigned int _number_of_points; // number of points in the cloud
     unsigned int _min_points;       // minimum number of points that must give connsensus
              
     /* Cloud of points */
     std::vector<Point> points;  // data
     std::vector<Point> inliers; // found inliers at each try
     
     /* 
        Vector containing the parameters of all the detected lines
        
        quality factor (the lower, the better) 
        slope, 
        intercept, 
        number of fitted points,
        total energy
     */
     std::vector<std::array<double, 5> > lines;

 public:
     //default constructor
     Ransac();
     
     // constructor and destructor
     Ransac(TH2F *data, double max_distance, int max_iterations, int n_tries, int min_num_points);
     virtual ~Ransac();
          
     // setters
     void set_data(TH2F *data);
    
     inline void set_max_distance(double max_distance)
     	{_d_max = max_distance;}
          	
     inline void set_max_iterations(int max_iterations)     
     	{_max_iterations = max_iterations;}
     
     inline void set_tries(int n_tries)
          {_n_tries = n_tries;}
     	
     inline void set_min_num_points(int min_num_points) 
     	{_min_points = min_num_points;}
     
     // getters 
     double get_max_distance() 
     	{return _d_max;}
     	
     double get_max_iterations() 
     	{return _max_iterations;}
     	
     double get_min_num_points() 
     	{return _min_points;}
     
     // get_ a random (non-zero) point from the cloud
     Point get_random_point();
     
     // searching for inliers using consensus
     void search_inliers(); 
     
     // remove the inliers associated to the detected line
     void remove_inliers(); 
                                            
     // get the lines found with RANSAC
     std::vector<std::array<double, 5> > get_lines();
     
     // print the parameters of the detected lines
     void print_lines();
     
     ClassDef(Ransac, 1);
};

#endif
