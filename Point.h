/*
  simple implementation of a point in 2D with a "weight" (energy)
*/

#include <array>
#include <iostream>
#include <utility>

#ifndef POINT_H
#define POINT_H

class Point : public TObject
{
 private:
     double _x, _y; // x & y coordinate
     double _e;     // energy 

 public:
     Point();
     
     Point(double x, double y, double e);
          
     ~Point(){};
     
     void set_x(double x) {_x = x;}
     void set_y(double y) {_y = y;}
     void set_e(double e) {_e = e;}
      
     void set_xye(double x, double y, double e);
     
     double get_x() {return _x;}
     double get_y() {return _y;}
     double get_e() {return _e;}
     
     std::array<double, 3> get_xye() {return {_x, _y, _e};}
        
     void operator = (Point const &p)
     { 
      _x = p._x;
      _y = p._y;
      _e = p._e;
     }  
          
     bool operator < (const Point &p) const {return _e < p._e;}
  
     bool operator == (const Point &p) const {return (_x == p._x) && (_y == p._y) && (_e == p._e);}
     
     friend std::ostream& operator<<(std::ostream &out, const Point &p)
     {
      out<<"("<<p._x<<", "<<p._y<<", "<<p._e<<")";
      return out;
     }

     // 2D line passing through two points
     /* m, q */
     static std::array<double,2> compute_simple_line(Point p1, Point p2); 
     
     // euclidean distance    
     static double distance(Point p, double m, double q);
     
     // calculation of the chi-squared of a detected line
     static double get_quality(std::vector<Point> inliers, double m, double q);       
     
     // fast linear fit and total energy calculation using the inliers
     /* 
        quality factor (the lower, the better) 
        slope, 
        intercept, 
        number of fitted points 
        total energy
     */
     static std::array<double, 5> fit_inliers(vector<Point> inliers);
     
     //fast vertex fitter
     /*
        the vertex is the point 
        minimizing the sum of the (squared) distance
        from each detected line        
        --> same result in case of two non-parallel lines
        --> best compromise for three or more lines
     */
     /*
        quality factor (the lower, the better)  --> not used
        slope, 
        intercept, 
        number of fitted points, --> not used
        total energy  --> not used
     */
     static std::pair<double, double> vertex_fitter(std::vector<std::array<double, 5> > lines);
     
     ClassDef(Point, 1);
};

#endif
