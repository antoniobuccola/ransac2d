// Ransac class implementation

#include "Ransac.h"

#include "TRandom3.h"
#include "TMath.h"

using namespace TMath;

#include "Point.h"

ClassImp(Ransac)

Ransac::Ransac()
{
 _d_max = 0;
 _max_iterations = 0;
 _n_tries = 0;
 _min_points = 0;
     
 points = {}; 
 inliers = {}; 
     
 lines = {};
}

Ransac::Ransac(TH2F* data, double max_distance, int max_iterations, int n_tries, int min_num_points)
{
 set_data(data);
  
 _d_max = max_distance;
 
 _number_of_points = points.size();
 _max_iterations = max_iterations;
 _n_tries = n_tries;
 _min_points = min_num_points;
 
 inliers = {};     
 lines = {};
 
 #if DBGLV > 0
     cout<<"Load successful!\n";
 #endif
}

/***************************************************************************/
/***************************************************************************/

Ransac::~Ransac(){}

/***************************************************************************/
/***************************************************************************/

void Ransac::set_data(TH2F* data)
{
 int nbinsx = data->GetXaxis()->GetNbins();
 int nbinsy = data->GetYaxis()->GetNbins();
 
 for(int xx = 1; xx <= nbinsx; xx++)
 {
  for(int yy = 1; yy <= nbinsy; yy++)
  {
   int cbin = data->GetBin(xx, yy);
   
   double c = data->GetBinContent(cbin);
   if(c>0)
   {      
    double x = shift + (xx-1)*(pad_size+pad_pitch);
    double y = shift + (yy-1)*(pad_size+pad_pitch);
    double e = c;
    
    Point p(x,y,e);
    
    #if DBGLV > 0
        cout<<p<<endl;
    #endif
    
    points.push_back(p);
   }
  }
 }
 
 #if DBGLV > 0
     cout<<"Data set\n";
     cout<<"Points: "<<points.size()<<endl;
 #endif
 
 _number_of_points = points.size();
}
 
/***************************************************************************/
/***************************************************************************/

Point Ransac::get_random_point()
{
 #if DBGLV > 0
     cout<<"Points: "<<points.size()<<"\n";
     cout<<"Num. of points: "<<_number_of_points<<endl;
 #endif
 TRandom3 x(0);
 int index = (int)(x.Uniform(0,_number_of_points)+1);
 #if DBGLV > 0 
     cout<<"--> Point chosen: "<<points[index]<<endl;
 #endif
 return points[index];
}

/***************************************************************************/
/***************************************************************************/

void Ransac::search_inliers() 
{
 lines = {};
 #if DBGLV > 0
     std::cout<<"Searching inliers\n";
     std::cout<<"Number of points: "<<_number_of_points<<endl;
     std::cout<<"Minimum number of points: "<<_min_points<<endl;
 #endif
 int tries = _n_tries;
 
 while(tries>=0 && _number_of_points >= _min_points)
 {
  #if DBGLV > 0
  	cout<<"\n**** Tries left: "<<tries<<endl;
  #endif
  double chi2 = 1e6;
  bool found_inliers = false;

  int iterations = _max_iterations;  
  while(iterations >= 0)
  {  
   #if DBGLV > 0
  	cout<<"Iterations left: "<<iterations<<endl;
   #endif 
   Point p1 = get_random_point();
   Point p2;
   do
     p2 = get_random_point();
   while(p1 == p2);
   
   std::array<double, 2> line = Point::compute_simple_line(p1, p2);
   
   std::vector<Point> maybe_inliers = {};
   double maybe_chi2 = 0;
  
   for(Point p : points)
   {
    double d = Point::distance(p, line[0], line[1]);
    if(d<=_d_max)
    {
     maybe_inliers.push_back(p);	
     maybe_chi2 += d*d;
    }
   }
   
   if(maybe_inliers.size()>=_min_points) // reached enough consensus
   {
    if(maybe_chi2 < chi2)  // got better inliers
    {
     found_inliers = true;
     inliers = maybe_inliers;
     chi2 = maybe_chi2;
    }
   }
  
   iterations--;
  }
  // end loop on iterations
    
  if(found_inliers)
  {
   #if DBGLV > 0
      cout<<"Number of inliers found: "<<inliers.size()<<endl;
   #endif
   lines.push_back(Point::fit_inliers(inliers));
   remove_inliers();
  }
  
  tries--; 
 }
 // end loop on tries
}

/***************************************************************************/
/***************************************************************************/

void Ransac::remove_inliers()	
{
 // this function is called if inliers were found 
 #if DBGLV > 0
     std::cout<<"Removing inliers\n";
 #endif
 for(Point P : inliers) 
 {
  for(Point Q : points)
  {
   if(P == Q)
   {
    // an inlier has been found and it will be removed from the actual points
    points.erase(std::remove(points.begin(), points.end(), Q), points.end());
    break;
   }
  }
  //loop on points end
 }
 // loop on inliers end
}
          
/***************************************************************************/
/***************************************************************************/

std::vector<std::array<double, 5> > Ransac::get_lines()
{
 #if DBGLV > 0
     cout<<"Found "<<lines.size()<<" lines"<<endl;
 #endif
 return lines;
}

/***************************************************************************/
/***************************************************************************/

void Ransac::print_lines()
{
 cout<<"*******************************\n";
 cout<<"        Found "<<lines.size()<<" line(s) "<<endl;
 cout<<"*******************************\n";

 if(lines.size()!=0)
 {
  for(auto l : lines)
  {
   cout<<"\n----------------\n";
   cout<<"Quality: "<<l[0]<<endl;
   cout<<"Slope: "<<l[1]<<endl;
   cout<<"Intercept: "<<l[2]<<endl;
   cout<<"Number of Points: "<<(int)l[3]<<endl;
   cout<<"Charge: "<<l[4]<<endl;
  } 
 }
}
