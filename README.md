Tracking algorithm in two dimensions

Class description

Point
     simple class modelling a point in the 2D space carrying an "energy"

Ransac: RANdom SAmpling Consensus model.
        Reference: https://dl.acm.org/doi/10.1145/358669.358692

Hough: Hough transform
       Reference: https://dl.acm.org/doi/10.1145/361237.361242
