// Point class implementation

#include "Point.h"

#include "TMath.h"

using namespace TMath;

ClassImp(Point);

Point::Point()
{
 _x = -1;
 _y = -1;
 _e = -1;
}

Point::Point(double x, double y, double e)
{
 _x = x;
 _y = y;
 _e = e;
}

void Point::set_xye(double x, double y, double e)
{
 _x = x;
 _y = y;
 _e = e;     
}

/***************************************************************************/
/***************************************************************************/

double Point::distance(Point p, double m, double q)
{
 double x = p.get_x();
 double y = p.get_y();
 
 return Abs(y - m*x -q)/(Sqrt(1+m*m));
}

/***************************************************************************/
/***************************************************************************/

double Point::get_quality(std::vector<Point> inliers, double m, double q)
{
 double chi2 = 0;
 
 for(auto &point: inliers) 
 {
  double d = distance(point, m, q);
  chi2 += d*d;
 }
 
 chi2 /= inliers.size();
 
 return chi2;
}   
 
/***************************************************************************/
/***************************************************************************/

std::array<double, 2> Point::compute_simple_line(Point p1, Point p2)
{
 double x1 = p1.get_x();
 double y1 = p1.get_y();
 
 double x2 = p2.get_x();
 double y2 = p2.get_y();
 
 double m = (y1-y2)/(x1-x2);
 double q = y1 - m*x1;
 
 return {m,q};
}

/***************************************************************************/
/***************************************************************************/

// quality, slope, intercept, total energy
std::array<double, 5> Point::fit_inliers(std::vector<Point> inliers)
{
 /* Weighted Least Square regression */
 double Sxy = 0;
 double Sxx = 0;
 double Sx  = 0;
 double Sy  = 0;
 double Sp  = 0; 
 
 double energy = 0; // i.e. the total charges
 
 for(auto point: inliers)
 {
  double x = point.get_x();
  double y = point.get_y();
  double e = point.get_e();
  
  Sxx += e*e*x*x;
  Sxy += e*e*x*y;
  Sx  += e*e*x;
  Sy  += e*e*y;
  Sp  += e*e;
  
  energy += e;
 }
 
 double D  = Sxx*Sp - Sx*Sx;
 double Dm = Sxy*Sp - Sx*Sy;
 double Dq = Sxx*Sy - Sx*Sxy;
 
 double m = Dm/D;
 double q = Dq/D;
 
 double chi2 = get_quality(inliers, m, q);
  
 return {chi2, m, q, (double)inliers.size(), energy};
}
        
/***************************************************************************/
/***************************************************************************/

/* 
  lines: vector containing the parameters of all the detected lines
        
         quality factor (the lower, the better) 
         slope, 
         intercept, 
         number of fitted points,
         total energy
*/
std::pair<double, double> Point::vertex_fitter(std::vector<std::array<double, 5> > lines)
{
 if(lines.size()<=1)
     return make_pair(-1,-1);
 
 double Smm = 0;
 double Smq = 0;
 double Sm  = 0; 
 double Sq  = 0;
 double S0  = 0;
 
 for(auto line : lines)
 {
  double m = line[1];
  double q = line[2];
  
  double d = 1 + m*m;
  
  Smm += m*m/d;
  Smq += m*q/d;
  Sm  += m/d;
  Sq  += q/d;
  S0  += 1./d;
 }
 
 double D  = Smm*S0 - Sm*Sm;
 if(Abs(D) < 1e-6) // i.e. lines are (almost) parallel and no vertex can be found
     return  make_pair(-1,-1);
 double Dx = Sm*Sq - Smq*S0;
 double Dy = Smm*Sq - Smq*Sm;
 
 double xv = Dx/D;
 double yv = Dy/D;
  
 return make_pair(xv, yv);
}


